# Booster Base POM

## Purpose

This project serves as a common baseline for all booster
projects by specifying common dependencies and versions.

## Usage

### Maven

Import the project in ```dependencyManagement``` and use in
```dependencies``` section the needed libraries.

Example

```xml
<project>
    <dependencies>
        <dependency>
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
            </dependency>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>io.gitlab.booster</groupId>
                <articleId>booster-base-pom</articleId>
                <type>pom</type>
                <scope>import</scope>
                <version>1.0.0</version>
            </dependency>
        </dependencies>
    </dependencyManagement>
</project>
```
